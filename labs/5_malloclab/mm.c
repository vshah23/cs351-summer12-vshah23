#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>

#include "mm.h"
#include "memlib.h"

#define ALIGNMENT 8

#define ALIGN(size) (((size) + (ALIGNMENT-1)) & ~0x7)

#define SIZE_T_SIZE (ALIGN(sizeof(size_t)))

#define BLK_HDR_SIZE ALIGN(sizeof(blockHdr))

typedef struct header blockHdr;

struct header {
  size_t size;
  blockHdr *next_p;
  blockHdr *prior_p;
};

void *find_firstfit(size_t size);
void *find_bestfit(size_t size);
void print_heap();
void split(blockHdr *bp, size_t size);
void coalesce();

//initializes the blockHdr at mem_heap_lo();
int mm_init(void)
{
    blockHdr *p = mem_sbrk(BLK_HDR_SIZE);
    p->size = BLK_HDR_SIZE;
    p->next_p = p;
    p->prior_p = p;
    return 0;
}

//prints the heap in a format useful for debugging
void print_heap() {
    blockHdr *bp = mem_heap_lo();
    while (bp < (blockHdr *)mem_heap_hi()) {
        printf("%s block at %p, size %d\n",
               (bp->size&1)?"allocated":"free",
               bp,
               (int)(bp->size & ~1));
        bp = (blockHdr *)((char *)bp + (bp->size & ~1));
    }
}

//allocates the given number of bits and returns a pointer to them
void *mm_malloc(size_t size)
{
    int newsize = ALIGN(BLK_HDR_SIZE + size);
    blockHdr *bp = find_bestfit(newsize);
    
    if (bp == NULL) {
        bp = mem_sbrk(newsize);
        if ((long)bp == -1)
            return NULL;
        else
            bp->size = newsize | 1;
    } else {
      if (bp->size > newsize + BLK_HDR_SIZE) //if the size of the block returned by best fit is too large
	split(bp, newsize);                  //split it
      else{
	bp->size |= 1;
	bp->prior_p->next_p = bp->next_p;
	bp->next_p->prior_p = bp->prior_p;
      }
    }
    return (char *)bp + BLK_HDR_SIZE;
}

//finds best fit in existing free blocks given size
void *find_bestfit(size_t size)
{
  blockHdr *p, *best_fit = NULL;
  size_t bsize = -1;

  for (p = ((blockHdr *)mem_heap_lo())->next_p; p != mem_heap_lo(); p = p->next_p){
    if (p->size >= size && bsize == -1){
      best_fit = p;
      bsize = p->size;
    }
    else if (p->size >= size && p->size <= bsize){
      best_fit = p;
      bsize = p->size;
    }

    if (bsize == size)
      return best_fit;
  }
  return best_fit;
}

//finds first fit in existing free blocks gived size (just here for comparison really -- not used)
void *find_firstfit(size_t size)
{
  blockHdr *p;
  for (p = ((blockHdr *)mem_heap_lo())->next_p;
       p != mem_heap_lo() && p->size < size;
       p = p->next_p);

  if (p != mem_heap_lo())
    return p;
  else
    return NULL;
}

/*
 *if the size of the block determined by bestfit was too large this function is called to split it
 *by reducing the size of the block to the desired size and then creating a new free block with the
 *remaining size
 */
void split(blockHdr *bp, size_t size)
{
  blockHdr *newblk = (blockHdr *)((char *)bp + ALIGN(size));
  newblk->size = bp->size - size;
  newblk->size &= ~1;
  bp->size = size;
  bp->size |= 1;
  bp->prior_p->next_p = newblk;
  bp->next_p->prior_p = newblk;
  newblk->next_p = bp->next_p;
  newblk->prior_p = bp->prior_p;
}

//same free as in mm-explicit.c
void mm_free(void *ptr)
{
  blockHdr *bp = ptr-BLK_HDR_SIZE,
    *head = mem_heap_lo();
  bp->size &= ~1;
  bp->next_p  = head->next_p;
  bp->prior_p = head;
  head->next_p = bp;
  bp->next_p->prior_p = bp;

  //coalesce(bp); //works but drops throughput to 0 since footers aren't implemented
}

/*
 *Coalescing, the bane of my existence. What this function does is looks at the block next to the
 *one passed in as a parameter. If the block and its next_p are within the heap and the block is not allocated
 *the function updates bp's size and removes bp's adjacent block form the free list.
 *The function then goes on to find the block previous to bp in the heap and once found it checks if it has been allocated.
 *If not, the previous block's size is incremented by bp's size and bp is then removed from the list of free blocks.
 *As a minor throughput boost once the previous block is found whether or not it is allocated the loop will break so the entire
 *heap isn't searched every time.
 */
void coalesce(blockHdr *bp)                                   //note: bp will always be the blockHdr right after head in our free list
{
  blockHdr *temp = (blockHdr *)((char *)bp + (bp->size & ~1));//temp is next block in heap

  if (temp > (blockHdr *)mem_heap_lo() &&
      temp < (blockHdr *)mem_heap_hi() &&
      temp->next_p > (blockHdr *)mem_heap_lo()               //check boundaries for temp and temp->next_p
      && temp->next_p < (blockHdr *)mem_heap_hi()){
    if (!(temp->size & 1)){                                  //if temp is not allocated
      bp->size += temp->size;                                //incr bp's size by temp's size
      temp->next_p->prior_p = temp->prior_p;
      temp->prior_p->next_p = temp->next_p;
    }
  }

  temp = ((blockHdr *)mem_heap_lo())->next_p;                //start search at block after mem_heap_lo()

  while (temp < (blockHdr *)mem_heap_hi()){
    if (((char*)temp + temp->size) == (char *)(bp)){
      if (!(temp->size & 1)){                                //if temp is not allocated
        temp->size += bp->size;                              //incr temp's size by bp
	bp->next_p->prior_p = bp->prior_p;
	bp->prior_p->next_p = bp->next_p;

      }
      break;                                                 //break since the block previous to bp in the heap was found
    }
    else
      temp = (blockHdr *)((char *)temp + (temp->size & ~1)); //if we didn't have the correct block go to the next one
  }
}

//same as in mm-explicit.c, will definitely revisit in next few days after
//throughput is improved
void *mm_realloc(void *ptr, size_t size)
{
   blockHdr *bp = ptr-BLK_HDR_SIZE;
    void *newptr = mm_malloc(size);
    if (newptr == NULL)
        return NULL;
    int copySize = bp->size-BLK_HDR_SIZE;
    if (size < copySize)
        copySize = size;
    memcpy(newptr, ptr, copySize);
    mm_free(ptr);
    return newptr;
}

//Question: How can we tweak the program for individual traces?
//I notice my last couple trace scores are poor in both utilization
//and throughput and that's all that was really holding me back.
//Was it just a matter of implementing coalescing efficiently (with footers)?
