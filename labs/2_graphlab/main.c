#include <stdlib.h>
#include <stdio.h>
#include "graph.h"

int main (int argc, char *argv[]) {
  vertex_t *p_head = NULL, *v1 = NULL, *v2 = NULL;
  int i = 1, distance, num_cities = 0;
  char *city1 = NULL, *city2 = NULL;

  while (argv[i] != NULL && argv[i+1] != NULL && argv[i+2] != NULL){
    city1 =  argv[i];
    city2 = argv[i+1];
    distance = atoi(argv[i+2]);

    if (vert_exists(p_head, city1) == 0){
      v1 = add_vert(&p_head, city1);
      num_cities++;
    }
    else
      v1 = find_vert(p_head, city1);

    if (vert_exists(p_head, city2) == 0){
      v2 = add_vert(&p_head, city2);
      num_cities++;
    }
    else
      v2 = find_vert(p_head, city2);

    add_edge(v1, v2, distance);
    add_edge(v2, v1, distance);

    i+=3;
  }
  
  print_adj(p_head);
  tour(p_head, num_cities);
  free_verts(p_head);
  
  return 0;
}
