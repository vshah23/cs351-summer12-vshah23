#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "graph.h"

/* implement the functions you declare in graph.h here */

void add_edge (vertex_t *city1, vertex_t *city2, int distance) {
  adj_vertex_t *p_adj = NULL;

  if (city1->adj_list == NULL){
    city1->adj_list = malloc(sizeof(adj_vertex_t));
    city1->adj_list->vertex = city2;
    city1->adj_list->edge_weight = distance;
    city1->adj_list->next = NULL;
  }
  else{
    p_adj = city1->adj_list;
    
    while (p_adj->next != NULL){
      p_adj = p_adj->next;
    }
    
    p_adj->next = malloc(sizeof(adj_vertex_t));
    p_adj = p_adj->next;
    p_adj->vertex = city2;
    p_adj->edge_weight = distance;
    p_adj->next = NULL;
  }
}

vertex_t * add_vert (vertex_t **p_head, char *city){
  vertex_t *p_temp = *p_head;

  if (*p_head == NULL){
    *p_head = malloc(sizeof(vertex_t));
    (*p_head)->name = city;
    return *p_head;
  }
  else {
    while (p_temp->next != NULL){
      p_temp = p_temp->next;
    }
    p_temp->next = malloc(sizeof(vertex_t));
    p_temp = p_temp->next;
    p_temp->name = city;
    return p_temp;
  }
}

vertex_t * find_vert (vertex_t *p_head, char *city){
  vertex_t *v = p_head;

  if (p_head == NULL)
    return NULL;
  else if (strncmp(p_head->name, city, strlen(city)) == 0)
    return p_head;
  else{
    while (v->next != NULL){
      v = v->next;
      if (strncmp(v->name, city, strlen(city)) == 0)
	return v;
    }
  }
  return NULL;
}

void free_adj (adj_vertex_t *adj){
  adj_vertex_t *p_adj = adj, *next = NULL;

  while (p_adj != NULL){
    next = p_adj->next;
    free(p_adj);
    p_adj = next;
  }
}

void free_verts (vertex_t *p_head){
  vertex_t *p_temp = p_head, *next = NULL;

  if (p_head == NULL);
  else{
    while (p_temp != NULL){
      free_adj(p_temp->adj_list);
      next = p_temp->next;
      free(p_temp);
      p_temp = next;
    }
  }
}

void print_adj (vertex_t *p_head){
  vertex_t *p_temp = p_head;
  adj_vertex_t *p_adj = NULL;

  if (p_head == NULL){
    printf("No vertices were defined!\n");
  }
  else{
    printf("Adjacency List:\n");
    while (p_temp != NULL){
      p_adj = p_temp->adj_list;
      printf("%s:", p_temp->name);
      while (p_adj != NULL){
	printf(" %s(%u)", p_adj->vertex->name, p_adj->edge_weight);
	p_adj = p_adj->next;
      }
      printf("\n");
      p_temp = p_temp->next;
    }
    printf("\n");
  }
}

int vert_exists (vertex_t *p_head, char *city){
  vertex_t *p_temp = p_head;

  while (p_temp != NULL){
    if (strcmp(p_temp->name, city) == 0)
      return 1;
    else
      p_temp = p_temp->next;
  }
  return 0;
  
}

int print_tour (vertex_t *p_head, int rem_cities, int dist){
  vertex_t *p_temp = p_head;
  adj_vertex_t *p_adj = NULL;
  int distance = 0;

  if (p_temp->visited != 0)
    return 0;
  else if (rem_cities == 0){
    printf("%s ", p_temp->name);
    return dist;
  }
  else{
      p_temp->visited = 1;
      p_adj = p_temp->adj_list;
      while (p_adj != NULL){
	distance = print_tour(p_adj->vertex, rem_cities-1, p_adj->edge_weight);
	if (distance == 0)
	  p_adj = p_adj->next;
	else{
	  printf("%s ", p_temp->name);
	  return (distance+dist);
	}
      }
      p_temp->visited = 0;
  }
  return 0;
}

void tour (vertex_t *p_head, int num_cities){
  vertex_t *p_temp = p_head;
  int tour_dist = 0, counter = 10;

  printf("Tour Path:\n");

  while (p_temp != NULL && counter != 0){
    tour_dist = print_tour(p_temp, num_cities-1, 0);

    if (tour_dist == 0)
      p_temp = p_temp->next;
    else
      break;

    counter--;
  }
  if (tour_dist == 0)
    printf("There is no tour!\n");
  else
    printf("\n\nTour length: %u\n", tour_dist);
}
