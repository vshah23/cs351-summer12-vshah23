/* 
 * hello.c - the quintessential hello world program
 */
#include <stdio.h>

int main () {
  printf("Hello world!\nVikas Shah\nA20262245\nI was thinking about how people seem to read the Bible a whole lot more as they get older. Then it dawned on me… they were cramming for their finals.\n");
  return 0;
}
