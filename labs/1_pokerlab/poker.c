#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "poker.h"

/* converts a hand (of 5 cards) to a string representation, and stores it in the
 * provided buffer. The buffer is assumed to be large enough.
 */
void hand_to_string (hand_t hand, char *handstr) {
    char *p = handstr;
    int i;
    char *val, *suit;
    for (i=0; i<5; i++) {
        if (hand[i].value < 10) {
            *p++ = hand[i].value + '0';
        } else {
            switch(hand[i].value) {
            case 10: *p++ = 'T'; break;
            case 11: *p++ = 'J'; break;
            case 12: *p++ = 'Q'; break;
            case 13: *p++ = 'K'; break;
            case 14: *p++ = 'A'; break;
            }
        }
        switch(hand[i].suit) {
        case DIAMOND: *p++ = 'D'; break;
        case CLUB: *p++ = 'C'; break;
        case HEART: *p++ = 'H'; break;
        case SPADE: *p++ = 'S'; break;
        }
        if (i<=3) *p++ = ' ';
    }
    *p = '\0';
}

/* converts a string representation of a hand into 5 separate card structs. The
 * given array of cards is populated with the card values.
 */
void string_to_hand (const char *handstr, hand_t hand) {
  int i=0, j=0;
  for (j; j<5; j++, i+=2){ //j=>card counter and i=>string position
      switch(handstr[i]) {
      case '2': hand[j].value = 2; break;
      case '3': hand[j].value = 3; break;
      case '4': hand[j].value = 4; break;
      case '5': hand[j].value = 5; break;
      case '6': hand[j].value = 6; break;
      case '7': hand[j].value = 7; break;
      case '8': hand[j].value = 8; break;
      case '9': hand[j].value = 9; break;
      case 'T': hand[j].value = 10; break;
      case 'J': hand[j].value = 11; break;
      case 'Q': hand[j].value = 12; break;
      case 'K': hand[j].value = 13; break;
      case 'A': hand[j].value = 14; break;
      }

      i++; //increment to get to suit char

    switch(handstr[i]){
    case 'D': hand[j].suit = DIAMOND; break;
    case 'C': hand[j].suit = CLUB; break;
    case 'H': hand[j].suit = HEART; break;
    case 'S': hand[j].suit = SPADE; break;
    }
  }
	   
}

/* sorts the hands so that the cards are in ascending order of value (two
 * lowest, ace highest */
void sort_hand (hand_t hand) {
  int i, j;

  for (i=0; i<5; i++){
    for(j=i+1; j<5; j++){
      if(hand[i].value > hand[j].value){
	swap(hand, i, j);
      }
    }
  }
}

void swap(hand_t hand, int a, int b){
  int temp = hand[a].value;
  suit_t tux = hand[a].suit;

  hand[a].value = hand[b].value;
  hand[a].suit = hand[b].suit;
  hand[b].value = temp;
  hand[b].suit = tux;
}

//sorts the given hand then counts the pairs
int count_pairs (hand_t hand) {
  int i, pairs=0;

  sort_hand(hand);

  for (i=0; i<4; i++){
    if (hand[i].value == hand[i+1].value){
      pairs++;
      i++;
    }
  }
    return pairs;
}

int is_onepair (hand_t hand) {
  return (count_pairs(hand) >= 1);
}

int is_twopairs (hand_t hand) {
  return (count_pairs(hand) == 2);
}

int is_threeofakind (hand_t hand) {
  int i;

  sort_hand(hand);

  for (i=0; i<3; i++){
    if ((hand[i].value == hand[i+1].value) && (hand[i].value == hand[i+2].value))
      return 1;
  }
    return 0;
}

int is_straight (hand_t hand) {
  int i;

  sort_hand(hand);

  if (hand[4].value == 14 && hand[0].value == 2){
    for (i=0; i<3; i++){
      if ((hand[i].value != hand[i+1].value-1))
	return 0;
    }
  }
    else{
      for (i=0; i<4; i++){
	if ((hand[i].value != hand[i+1].value-1))
	  return 0;
      }
    }
    return 1;
}

int is_fullhouse (hand_t hand) {
  int i;

  sort_hand(hand);

  if ((hand[0].value == hand[1].value) && (hand[0].value == hand[2].value)){
    if (hand[3].value == hand[4].value)
	return 1;
  }
    else if ((hand[2].value == hand[3].value) && (hand[2].value == hand[4].value)){
      if (hand[0].value == hand[1].value)
	return 1;
    }	
    return 0;
}

int is_flush (hand_t hand) {
  int i;
  
  sort_hand(hand);

  for (i=0; i<4; i++){
    if (hand[i].suit != hand[i+1].suit)
      return 0;
  }
    return 1;
}

int is_straightflush (hand_t hand) {

  if (is_flush(hand) && is_straight(hand))
    return 1;

    return 0;
}

int is_fourofakind (hand_t hand) {

  sort_hand(hand);

  if (hand[0].value == hand[1].value && hand[1].value == hand[2].value && hand[2].value == hand[3].value)
    return 1;
  else if (hand[1].value == hand[2].value && hand[2].value == hand[3].value && hand[3].value == hand[4].value)
    return 1;
  else
    return 0;
}

int is_royalflush (hand_t hand) {
  if (is_straight(hand) && is_flush && (hand[4].value == 14))
    return 1;

    return 0;
}

/* compares the hands based on rank -- if the ranks (and rank values) are
 * identical, compares the hands based on their highcards.
 * returns 0 if h1 > h2, 1 if h2 > h1.
 */
int compare_hands (hand_t h1, hand_t h2) {
  if (is_royalflush(h1))
    return 0;
  else if (is_royalflush(h2))
    return 1;
  else if (is_straightflush(h1)){
    if (is_straightflush(h2))
	return compare_highcards(h1,h2);
    else
      return 0;
  }
  else if (is_straightflush(h2))
    return 1;
  else if (is_fourofakind(h1)){
    if (is_fourofakind(h2))
      return compare_highcards(h1, h2);
    else
      return 0;
  }
  else if (is_fourofakind(h2))
    return 1;
  else if (is_fullhouse(h1)){
    if (is_fullhouse(h2))
	return compare_highcards(h1,h2);
    else
      return 0;  
  }
  else if (is_fullhouse(h2))
      return 1;
  else if (is_flush(h1)){
    if (is_flush(h2))
      return compare_highcards(h1,h2);
    else
      return 0;
  }
  else if (is_flush(h2))
    return 1;
  else if (is_straight(h1)){
    if (is_straight(h2))
      return compare_highcards(h1,h2);
    else
      return 0;
  }
  else if (is_straight(h2))
    return 1;
  else if (is_threeofakind(h1)){
    if (is_threeofakind(h2))
      return compare_highcards(h1,h2);
    else
      return 0;
  }
  else if (is_threeofakind(h2))
    return 1;
  else if (is_twopairs(h1)){
    if (is_twopairs(h2))
      return compare_highcards(h1,h2);
    else
      return 0;
  }
  else if (is_twopairs(h2))
    return 1;
  else if (is_onepair(h1)){
    if (is_onepair(h2))
      return compare_highcards(h1,h2);
    else
      return 0;
  }
  else if (is_onepair(h2))
    return 1;
  else
    return compare_highcards(h1, h2);
}

/* compares the hands based solely on their highcard values (ignoring rank). if
 * the highcards are a draw, compare the next set of highcards, and so forth.
 */
int compare_highcards (hand_t h1, hand_t h2) {
  int i;

  sort_hand(h1);
  sort_hand(h2);

  for (i=4; i>-1; i--){
    if (h1[i].value > h2[i].value)
      return 0;
    else if (h1[i].value < h2[i].value)
      return 1;
  }

}
